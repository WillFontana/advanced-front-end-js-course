// for (i = 0; i <= 100; i++ ){
//     console.log(i)
// }

// var j = 500
// while (j > 50){
//     console.log(j);
//     j/=5;
// }
// alert('Bem vindo ao curso de Java Script !')
// function alarme(){
//     document.querySelector('.title').style.color = 'red';
// }

// // setInterval(alarme, 1000);
// setTimeout(alarme, 3000);

// Exercício N° 1:

function localizar (endereco) {
    return (
        'O usuario mora na rua: ' +
        endereco.rua +
        ' N°' +
        endereco.numero +
        ', no bairro ' +
        endereco.bairro +
        ' localizado na zona norte de ' +
        endereco.cidade +
        ' no norte do' +
        endereco.uf +
        '.'
    );
}
var endereco = {
    rua: 'Manoel Carlos Ferraz de almeida',
    numero: 350,
    bairro: 'Jd. Império do Sol',
    cidade: 'Londrina',
    uf: 'Paraná'
};

console.log(localizar(endereco));

// Exercício n° 2:

function showEven(x, y){
    for (i = x; i <= y; i++){
        if (i % 2 === 0){
            console.log(i);
        }
    } 
}
showEven(0, 20);

// Exercício n° 3:
function hasSkill(skills){
    return skills.indexOf('JavaScript') !== -1;
}
var skills = ["JavaScript", "ReactJS", "React Native"];
console.log(hasSkill(skills));

//Exercício n° 4:

function experiencia(anos) {
    if (anos >= 0 && anos <= 1) {
        console.log('O usuario é iniciante');
    } else if (anos >= 1 && anos <= 3){
        console.log('O usuario é adepto');
    } else if (anos >= 3 && anos <= 6) {
        console.log('O usuario é avançado');
    } else if (anos >= 7) {
        console.log('O usuario é mestre');
    } else {
        console.log('Não foi possivel identificar o graudo usuario.');
    }
}
var anosEstudo = prompt('Ha quantos anos voce pratica e estuda?');
experiencia(anosEstudo)

//Exercício N° 5:
var usuarios = [{
    nome: 'Willyan',
    habilidades: ['JavaScript','React','Redux']
    },
    {
    nome: 'Bruno',
    habilidades: ['Vue', 'Framework', 'Elixir']
    }
];
function showSkills(usuarios){
    for (usuario of usuarios){
        console.log(
            "O " +
            usuario.nome +
            " possui as habilidades" +
            usuario.habilidades.join(", ") +
            " ."
        )
    }
}
showSkills(usuarios);
