function ceu() {
    var bodyElement = document.querySelector('#app');
    var boxElement = document.querySelector('#box');
    bodyElement.removeAttribute('class');
    bodyElement.setAttribute('class', 'ceu');
    boxElement.removeAttribute('class');
    boxElement.setAttribute('class', 'Box-ceu');
}
function terra() {
    var bodyElement = document.querySelector('#app');
    var boxElement = document.querySelector('#box');
    bodyElement.removeAttribute('class');
    bodyElement.setAttribute('class', 'terra');
    boxElement.removeAttribute('class');
    boxElement.setAttribute('class', 'Box-terra');
}
function inferno() {
    var bodyElement = document.querySelector('#app');
    var boxElement = document.querySelector('#box');
    bodyElement.removeAttribute('class');
    bodyElement.setAttribute('class', 'inferno');
    boxElement.removeAttribute('class');
    boxElement.setAttribute('class', 'Box-inferno');
}