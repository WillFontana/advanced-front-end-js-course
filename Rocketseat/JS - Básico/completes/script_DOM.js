// var inputElement = document.querySelector('#nome');
// var btnElement = document.querySelector('button.btn');

// containerElement.removeChild(inputElement);
// btnElement.onclick = function(){
//     var text = inputElement.value;
//     alert (text);
// }

// console.log(inputelement);


var linkElement = document.createElement('a');
linkElement.setAttribute('href', '#');
linkElement.setAttribute('class', 'link');
var textElement = document.createTextNode('EAE GALERA');
linkElement.appendChild(textElement);
var containerElement = document.querySelector('.Box')
containerElement.appendChild(linkElement);

function changeBG() {
    var bodyElement = document.querySelector('.body');
    var boxElement = document.querySelector('.Box');
    bodyElement.removeAttribute('class', 'body');
    bodyElement.setAttribute('class', 'body-alt')
    boxElement.style.backgroundColor = '#c5c4c4';
}

// Exercício 1:
function createBox() {
    var boxCreate = document.createElement('div');
    boxCreate.setAttribute('class', 'newBox');
    var boxElement = document.querySelector('.Box');
    boxElement.appendChild(boxCreate);
    //Exercício 2:
    boxCreate.onmouseover = function () {
        boxCreate.style.backgroundColor = getRandomColors();
    }

}
// Exercício 2:
function getRandomColors() {
    var letters = "0   123456789abcdef";
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
var newColor = getRandomColors();

// Exercício 3:

var nomes = ["Willyan", "Reginaldo", "Heynoan"];
var listElement = document.querySelector('ul');
    for (nome of nomes){
        var liElement = document.createElement('li');
        liElement.setAttribute('class', 'list-item')
        var textLi = document.createTextNode(nome)

        liElement.appendChild(textLi);
        listElement.appendChild(liElement);
    }

// Exercício 4:
var nomes = ["Matheus", "Danilo"];
var listElement = document.querySelector('ul');
var inputElement = document.querySelector('#nome');
    function addItem(text){
        var liElement = document.createElement('li');
        liElement.setAttribute('class', 'list-item')
        var textLi = document.createTextNode(text);

        liElement.appendChild(textLi);
        listElement.appendChild(liElement);
    }
    function adicionarLI(){
        addItem(inputElement.value);
        inputElement.value = ""; 
    }
    for (nome of nomes){
        addItem(nome)
    }