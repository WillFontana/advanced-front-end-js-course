var listElement = document.querySelector("#app ul");
var inputElement = document.querySelector("#app input");
var btnElement = document.querySelector("#app button");

var todos = JSON.parse(localStorage.getItem('list_todos')) || [];

function renderizarTodos() {
  listElement.innerHTML = "";
  for (todo of todos) {
    console.log(todo);
    var todoElement = document.createElement("li");
    var todoText = document.createTextNode(todo);

    var linkElement = document.createElement("a");
    var linkText = document.createTextNode("Excluir");
    linkElement.setAttribute("href", "#");

    var pos = todos.indexOf(todo);
    linkElement.setAttribute("onclick", "removeTodo(" + pos + ")");

    linkElement.appendChild(linkText);

    todoElement.appendChild(todoText);

    todoElement.appendChild(linkElement);
    listElement.appendChild(todoElement);
  }
}
renderizarTodos();

function addTodo() {
  var todoText = inputElement.value;
  if (todoText !== "") {
    todos.push(todoText);
    inputElement.value = "";
    renderizarTodos();
    storageTodo();
  } else {
    renderizarTodos();
  }
}
btnElement.onclick = addTodo;

function removeTodo(pos) {
  todos.splice(pos, 1);
  renderizarTodos();
}

function storageTodo(){
    localStorage.setItem('list_todos', JSON.stringify(todos));
}