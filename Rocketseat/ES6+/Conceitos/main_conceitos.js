// const arr = [1,2,3,4,5,6]

// const newArray = arr.map(function(item, index){
//   if(item >= 4){
//   return item * 2 + index;
// } else {
//   return item * 3 - index ;
// }
// })

// console.log(newArray);

// const sum = arr.reduce(function(total, next){
//   if(total <= 4){  
//   return total + next;
//   } else {
//     return total * next;

//   }
// })
// console.log(sum);

// const filter = arr.filter(function(item){
//   return item % 2 === 0;
// })
// console.log(filter);

// const finder = arr.find(function(item){
//   return item === 15;
// })

// console.log(finder);

///////////////////////////////////////////////////////////
// Arrow Function /////////////////////////////////////////
///////////////////////////////////////////////////////////

// const arr = [1,2,3,4,5,6];

// const newArray = arr.map(item => item * 2)
// console.log(newArray);

// const somaPadronizada = (a = 3, b = 6) => a + b;


// console.log(somaPadronizada(1));
// console.log(somaPadronizada());

///////////////////////////////////////////////////////////
// Desestruturação/////////////////////////////////////////
///////////////////////////////////////////////////////////

// const usuario = {
//     nome: 'Willyan',
//     idade: 20,
//     endereço:{
//         cidade: 'Londrina',
//         estado: 'PR',
//     },
// };

// const { nome, idade, endereço:{ cidade } } = usuario;

// function teste({nome}){
//  alert(nome);
// }

///////////////////////////////////////////////////////////
// Rest / Spread///////////////////////////////////////////
///////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////
// REST

// const usuario = {
//     nome: 'Willyan',
//     idade: 20,
//     trabalho: 'Front End'
// }

// const {nome , ...resto} = usuario;

// console.log(resto);

// const arr = [1,2,3,4];

// const [a, b, ...c] = arr;

// console.log(c)

// function soma (...params){
//      return params //.reduce((total, next) => total + next);
// }

// console.log(soma(5, 10, 4, 6, 7, 1, 8));


///////////////////////////////////////////////////////////
// SPREAD

// const arr1 = [1, 2, 3];
// const arr2 = [4, 5, 6];

// const arr3 = [...arr1, ...arr2];

// console.log(arr3);

// const usuario1 = {
//     nome: 'Willyan',
//     idade: 20,
//     trabalho: 'Front End'
// }

// const usuario2 = { ...usuario1, nome: 'Reginaldo', idade:19, trabalho: 'Designer'};

// console.log(usuario1);

// console.log(usuario2);

///////////////////////////////////////////////////////////
// Template Literals///////////////////////////////////////
///////////////////////////////////////////////////////////

// const nome = 'Willyan';
// const idade = 20;

// console.log('Meu nome é '+nome+' e tenho '+idade+' anos de idade.');
// console.log(`Meu nome é ${nome} e tenho ${idade} anos`)



//                                  EXERCÍCIOS

//  // 1°:

// class Usuario {
//     constructor(email, senha) {
//         this.email = email;
//         this.senha = senha;
//     }

//     isAdmin() {
//         return this.admin === true;
//     }
// }

// class Admin extends Usuario {
//     constructor(email, senha) {
//         super(email, senha);

//         this.admin = true
//     }
// }

// const user1 = new Usuario('WillFontana', 'senhaManeirassa');
// const admin1 = new Admin('WillFontana', 'senhaManeirassa');

// console.log(user1.isAdmin());
// console.log(admin1.isAdmin());

// // 2°:

// const usuarios = [
//     { nome: 'Willyan', idade: 20, trabalho: 'Front' },
//     { nome: 'Reginaldo', idade: 19, trabalho: 'Design' },
//     { nome: 'Guilherme', idade: 28, trabalho: 'Front' },
// ];

// // 2.1:
// const idades = usuarios.map(usuario => usuario.idade)
// console.log(idades);

// // 2.2:
// const fronts = usuarios.filter(usuario =>
//     usuario.trabalho === 'Front' && usuario.idade >= 18);
// console.log(fronts)

// // 2.3:
// const backs = usuarios.find(usuario =>
//     usuario.trabalho === 'BackEnd')
// console.log(backs)

// // 2.4:
// const aposentado = usuarios
//         .map(usuario => ({...usuario, idade: usuario.idade * 2 }))
//         .filter(usuario => usuario.idade >= 50);
// console.log(aposentado);

// // 3°:

// 3.1:

// const arr = [1, 2, 3, 4, 5];
// const newArray = arr.map((item) => {
//     return item + 10;
// })
// console.log(newArray);

// // 3.2:

// const usuario = { nome: "Diego", idade: 23 };
// const mostraIdade = usuario => usuario.idade;

// // 3.3:

// const name = 'Willyan';
// const age = 20;
// const mostraUsuario = (name = 'willyan', age = 20) => {
//     name,
//         age
// }
// mostraUsuario(name, age);
// mostraUsuario(name);

// // 3.4:
// const promise = () => new Promise((resolve, reject) => resolve());

// // 4°:

// 4.1:
// const empresa = {
//     nome: 'Dream',
//     endereco: {
//     cidade: 'Londdrina',
//     estado: 'PR',
//     }
//    };

// const {nome, endereco:{cidade, estado}} = empresa;

// console.log(nome);
// console.log(cidade);
// console.log(estado);

// // 4.2:

// function mostraInfo(nome, idade) {
//     console.log(`Meu nome é ${nome} e tenho ${idade} anos de idade`)
//    }

// mostraInfo({nome: Willyan, idade: 20});

// // 5°:

// // 5.1:

// const arr = [1, 2, 3, 4, 5, 6];

// const [a, ...b] = arr;

// console.log(a);
// console.log(b);

// function soma(...parametros) {
//     return parametros.reduce((total, next) => total + next);
// }
// console.log(soma(4, a, 5))

// // 5.2:
// const usuario = {
//     nome: 'Willyan',
//     idade: 20,
//     endereco: {
//         cidade: 'Londrina',
//         uf: 'PR',
//         pais: 'Brasil',
//     }
// };
// const usuario2 = {...usuario, nome: 'Gabriel'}
// const usuario3 = {...usuario.endereco, cidade: 'Lontras'}
// console.log(usuario2);
// console.log(usuario3);

// // 6°:

//     const nome = 'Willyan';
//     const idade = 20;

// console.log(`Meu nome é ${nome} e eu tenho ${idade} anos de idade`);

// // 7°:

// const nome = 'Willyan';
// const idade = 20;

// const usuario ={
//     nome,
//     idade,
//     empresa: 'dream'
// }

// console.log(usuario);