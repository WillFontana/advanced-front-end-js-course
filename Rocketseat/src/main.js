const minhaPromise = () => new Promise((resolve, reject) => {
    setTimeout(() => { resolve('OK') }, 2000)
});
const otherPromise = () => new Promise((resolve, reject) => {
    setTimeout(() => { resolve('Done') }, 1000)
});

// async function executarPromise(){
//     const response = await minhaPromise();
//     console.log(response);
//     console.log(await otherPromise());
//     console.log(await otherPromise());
//     console.log(await otherPromise());
//     console.log(await otherPromise());
// }

// executarPromise();

const executarPromise = () => {
    
}