
//////////////////////////////////////
// Criando objetos usando o construtor

// var Person = function(name,dataNascimento,trabalho){
//     this.name = name;
//     this.dataNascimento = dataNascimento;
//     this.trabalho = trabalho;
// }

// Person.prototype.calcularIdade = function(){
//     console.log(2019 - this.dataNascimento);
// };

// Person.prototype.lastName = 'Fontana';

// var will = new Person('Willyan', 1999, 'Front End');
// var regi = new Person('Reginaldo', 2000, 'Designer');
// var hey = new Person('Heynoan', 2001, 'Estudante');
// will.calcularIdade();
// regi.calcularIdade();
// hey.calcularIdade();

// console.log(will.lastName);
// console.log(regi.lastName);
// console.log(hey.lastName);

//////////////////////////////////
// Criando objetos: Objects.create

// var personProto = {
//     calcularIdade: function () {
//         console.log(2019 - this.dataNascimento)
//     }
// }

// var will = Object.create(personProto);
// will.nome = 'Willyan';
// will.dataNascimento = 1999;
// will.trabalho = 'Front End';

// var regi = Object.create(personProto,
//     { 
//         nome: { value: 'Reginaldo'},
//         dataNascimento: {value: 2000},
//         trabalho: {value: 'Designer'}
//     });

////////////////////////
// Primitive vs objects:

// Primitivos
// var a = 1;
// var b = a;
// a = 50;
// console.log(a, b);

// // Objetos
// var obj1 = {
//     nome: 'Willyan',
//     age: 20
// };
// var obj2 = obj1;

// obj1.age = 21;
// console.log(obj1);
// console.log(obj2);

// // Funções
// var age = 27;
// var obj = {
//     name:   'Willyan',
//     cidade: 'Londrina',
// }

// function change(a, b) {
//     a = 30;
//     b.cidade = 'São Paulo';
// }
// change(age, obj);


// console.log(age);
// console.log(obj.cidade);

// var years = [1990, 1965, 1937, 2016, 2003];

// function arrayCalc(arr, fn) {
//     var arrRes = [];
//     for (i = 0; i < arr.length; i++) 
//     {
//         arrRes.push(fn(arr[i]));
//     }
//     return arrRes;
// }

// function calcularIdade(el) {
//     return 2019 - el;
// }

// function deMaior(el){
//     return el >= 18;
// }

// function maxBatCardiaco(el){
//     if (el >= 18 && el <= 81){
//         return Math.round(206.9 - (0.67 * el));
//     } else {
//         return -1;
//     }
// }

// ages = arrayCalc(years, calcularIdade);
// mediaCardiaca = arrayCalc(ages, maxBatCardiaco);
// maioridade = arrayCalc(ages, deMaior);
// console.log(ages);
// console.log(maioridade);
// console.log(mediaCardiaca);

//////////////////////////////
// Funções retornando funções:

// function interviewQuestion(job) {
//     if (job === 'Front End') {
//         return function (name) {
//             console.log(name + ', por favor explique o que seria responsividade.');
//         }
//     } else if (job === 'Designer') {
//         return function (name) {
//             console.log(name + ', por favor explique o que seria UX design.');
//         }
//     } else if (job === 'Back End') {
//         return function (name) {
//             console.log(name + ', por favor explique o que é programação orientada a objetos.');
//         }
//     } else {
//         return function (name) {
//             console.log(name + ', desculpe mas não estamos ofertando essa vaga de emprego no momento');
//         }
//     }
// }

// var frontEnd = interviewQuestion('Front End');
// var designer = interviewQuestion('Designer');
// var backEnd = interviewQuestion('Back End');
// var none = interviewQuestion('Aleatorio');

// frontEnd('Willyan Fontana');
// designer('Reginaldo Rocha');
// backEnd('Rafael Cardonazio');
// none('Usuario');

//////////////////////////////////////////////////////////////////
// Invocação instantanea de expressão de funções  (IIEF) ou (IIFE)

// function game(){
//     var score = Math.random()*10;
//     console.log(score >= 5);
// }
// game();


//(function(){
//     var score = Math.random()*10;
//     console.log(score >= 5);
// })(); // Isso é um IIFE
// // console.log(score);


// (function(goodLuck){
//     var score = Math.random()*10;
//     console.log(score >= 5 - goodLuck);
// })(5); // Isso é um IIFE
// console.log(score);

/////////////
// Closures:

// function aposentadoria(idadeAposentadoria){
//     var a = ' anos até a aposentadoria.';
//     return function(dataNascimento){
//         var age = 2019 - dataNascimento;
//         console.log((idadeAposentadoria - age) + a);
//     }
// }

// var aposentadoriaBR = aposentadoria(65);
// aposentadoriaBR(1999);

// var aposentadoriaUS = aposentadoria(66);
// aposentadoriaUS(1999);

// var aposentadoriaUSA = aposentadoria(60);
// aposentadoriaUSA(1999);


// aposentadoria(70)(1990);
// aposentadoria(65)(1981);

/////////////
// Chalenge:

// function interviewQuestion(job) {
//     var a = ', por favor explique o que seria responsividade.'
//     return function (name) {
//         if (job === 'Front End') {
//             console.log(name + ', por favor explique o que seria responsividade.');
//         } else if (job === 'Designer') {
//             console.log(name + ', por favor explique o que seria UX design.');
//         } else if (job === 'Back End') {
//             console.log(name + ', por favor explique o que é programação orientada a objetos.');
//         } else {
//             console.log(name + ', desculpe mas não estamos ofertando essa vaga de emprego no momento');
//         }
//     }
// }
// interviewQuestion('Front End') ('Willyan');
// interviewQuestion('Designer') ('Reginaldo');
// interviewQuestion('Back End') ('Rafael');
// interviewQuestion('Agricultor') ('Jose');

////////////////////////
// Bind, Call and Apply

// var will = {
//     nome:           'Willyan',
//     idade:          20,
//     trabalho:       'front end',

//     apresentacao:   function(style, horario) {
    
//         if (style === 'formal'){
//             console.log('Boa ' + horario + ', eu me chamo '+ this.nome + ', minha idade é '+ this.idade +' anos e atuo na area de '+ this.trabalho +' no setor de desenvolvimento web.');
//         } else if (style === 'informal'){
//             console.log('Eae mano, boa '+ horario +' meu nome é '+ this.nome +', to com '+ this.idade +' anos e sou '+ this.trabalho +' .');
//         } else {
//             console.log('...');
//         }
//     }
// }

// var regi = {
//     nome: 'Reginaldo',
//     idade: 19,
//     trabalho: 'Designer'
// }

// var willFormal = will.apresentacao.bind(will, 'formal');
// willFormal('Tarde')
// willFormal('Noite')
// will.apresentacao('formal', 'noite');
// will.apresentacao.call(regi,'informal', 'tarde')





// var years = [1990, 1965, 1937, 2016, 2003];

// function arrayCalc(arr, fn) {
//     var arrRes = [];
//     for (i = 0; i < arr.length; i++) 
//     {
//         arrRes.push(fn(arr[i]));
//     }
//     return arrRes;
// }

// function calcularIdade(el) {
//     return 2019 - el;
// }

// function deMaior(limit, el){
//     return el >= limit;
// }

// var ages = arrayCalc(years, calcularIdade)

// var fullJapao = arrayCalc(ages, deMaior.bind(this, 20));
// console.log(fullJapao);
// console.log(ages);

// Coding Chalenge

function Questions(pergunta, resposta, certa){
    this.pergunta = pergunta;
    this.resposta = resposta;
    this.certa = certa;
}
var pergunta1 = new Questions('Você é gay?', 
['Não gosto de mulher com estria.', 'Queria estar fazendo uma rinha de galo ae'],
 0);
var pergunta2 = new Questions('Voce odeia o véio da Havan?', ['Odeio, sou comunistx, veio fedido','Ora pois, mal sabe que ele é um exemplo de desenvolvimento financeiro', 'imposto é roubo lulonaro 2006, binoliro presidente, loula vice'], 2);
var pergunta3 = new Questions('Meu amigo que dor de barriga né?', ['Nem comi', 'Buxim cheio', 'É nois bicho', 'Cagae poh'], 3)

var Aleatorio = [pergunta1, pergunta2, pergunta3];
var n = Math.floor(Math.random() * Aleatorio.length);
Aleatorio[n]