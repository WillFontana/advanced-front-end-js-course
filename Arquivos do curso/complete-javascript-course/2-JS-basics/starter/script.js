//Escrevendo infos no console: 
// console.log('Hello World!');

/*____________________________________________________________________________________________________________________________________________________________*/

//Declarando Variaveis:
// var firstName = "Will";

// console.log(firstName);

// var lastName = "Fontana";
// var age = 20;
// var fullAge = true; 
// console.log(fullAge);

// var job;

// console.log(job);

// job = "front-end";
// console.log(job);

/*____________________________________________________________________________________________________________________________________________________________*/

// coesão de tipos
// var firstName = 'Will';
// age = 20;
// var job, relachionchip;

// job = "front-end";
// // relachionchip = true;

// console.log(firstName + ' tem ' + age+ ' anos de idade, e é ' + job + ' seu relacionamento é ' + relachionchip);

/*____________________________________________________________________________________________________________________________________________________________*/

// Comandos de eventos :
// console.log('É um comando onde o texto é exibido no terminal do navegador');

// alert('É um comando onde o texto é exibido em uma caixa de dialogo no navegador');

// respostaDoPrompt = prompt('Seria esse o comando de uma caixa de pergunta do navegador?');

// alert (respostaDoPrompt);

/*____________________________________________________________________________________________________________________________________________________________*/

// Operadores matematicos:

// x = 10;
// y = 2;

// maior = x * y;
// menor = x + y;
// var alfa;
// vazio = null;

// Operadores lógicos:

// var eMaior = maior > menor;
// var eMenor = maior < menor;

// alert(eMaior);
// alert(eMenor);

// Operador typeOf:

// alert(typeof maior);
// alert(typeof eMaior);
// alert(typeof 'escrita');
// alert(typeof alfa);
// alert(typeof vazio);

/*____________________________________________________________________________________________________________________________________________________________*/

// Operadores Precedentes:

// anoAtual = 2019;
// dataNascimento = 1999;
// maioridade = 18;

// maoiridadeAtingida = anoAtual - dataNascimento >= maioridade; //true

// minhaIdade = anoAtual - dataNascimento;
// idadeMae = minhaIdade * 2;
// idadePrimo = minhaIdade - 5;

// idadeMedia =( minhaIdade + idadeMae + idadePrimo) / 3; // 25

// console.log(minhaIdade);
// console.log(idadeMae);
// console.log(idadePrimo);

// console.log(idadeMedia);


// Múltiplas atribuições:

// x = y = (3 + 5 ) * 4 - 6; // 8 * 4 - 6  // 32 - 6   // 26

// console.log(y + x, x, y);

// x *= 2;

// console.log(x);

/*____________________________________________________________________________________________________________________________________________________________*/

// // Will 
// MassaWill = 70; //Kg
// AlturaWill = 1.83;//Metros
// BMIWill = MassaWill/AlturaWill**2;

// // Regi
// MassaRegi = 64; //Kg
// AlturaRegi = 1.83; //Metros
// BMIRegi = MassaRegi/AlturaRegi**2;

// console.log(MassaWill);
// console.log(AlturaWill);
// console.log(BMIWill);

// console.log(MassaRegi);
// console.log(AlturaRegi);
// console.log(BMIRegi);

// BMIMaior = BMIWill > BMIRegi; 

// console.log('O BMI\' do Will é maior do que o BMI do Reginaldo? ' + BMIMaior);

/*____________________________________________________________________________________________________________________________________________________________*/

// if/else

// firstname = 'Will';
// civilStatus = 'relachionship';

// if (civilStatus === 'single') {
//     console.log(firstname + ' is single.');
// } else {
//     console.log(firstname + ' is in  a relachionship.');
// }

// isSingle = false;

// if (isSingle) {
//     console.log(firstname + ' is single.');
// } else {
//     console.log(firstname + ' is not single');
// }

/*____________________________________________________________________________________________________________________________________________________________*/

// // Desafio 1

// // Will 
// MassaWill = 70; //Kg
// AlturaWill = 1.83;//Metros
// BMIWill = MassaWill/AlturaWill**2;

// // Regi
// MassaRegi = 64; //Kg
// AlturaRegi = 1.83; //Metros
// BMIRegi = MassaRegi/AlturaRegi**2;

// if (BMIWill > BMIRegi) {
//     console.log('O BMI do Will é maior que o BMI do Regi');
// } else {
//     console.log('O BMI do Regi é maior que o BMI do Will')
// }

/*____________________________________________________________________________________________________________________________________________________________*/

// firstName = 'Will';
// age = '20';

// if (age < 13) {
//     console.log(firstName + ' is a boy');
// } else if (age < 18) {
//     console.log(firstName + ' is a teenager');
// } else if (age < 50) {
//     console.log(firstName + ' is an adult');
// } else {
//     console.log(firstName + ' is an old man');
// }

// if (age < 13) {
//     console.log(firstName + ' is a boy');
// } else if (age >= 13 && age < 20) {
//     console.log(firstName + ' is a teenager');
// } else if (age >= 20 && age < 50) {
//     console.log(firstName + ' is an adult');
// }

/*____________________________________________________________________________________________________________________________________________________________*/

/* O operador Ternary
*/

// firstName = 'Will';
// age = 20;
// drink = age >= 18

// age >= 18 ? console.log(firstName + ' drink alchool')
// : console.log(firstName + ' doesn\'t drink alchool');

// drink = age >= 18 ? 'Cerveja' : 'Suco';
// console.log(drink);

// age = prompt('Qual a sua idade');

// if (age >= 18) {
//     alert('Voce ja tem idade o bastante para beber');
// } else {
//     alert('Me desculpe mas menores de idade não podem beber');
// }

// Switch statement
// job = 'front-end';
// switch (job) {
//     case 'front-end': console.log(firstName + ' trabalha na area client-size do desenvolvimento web');
//         break;
//     case 'back-end': console.log(firstName + ' trabalha na area server-size do desenvolvimento web');
//         break;
//     case 'desingner': console.log(firstName + ' trabalha no desenvolvimento visual do site');    
//         break;
//     default : console.log('O trabalho do ' + firstName + ' é desconhecido');
// }

// firstName = prompt('Qual o seu nome?');
// age = prompt('Qual a sua idade?');


// switch(true){
//     case age < 13:
//     console.log(firstName + ' é uma criança!');
//     break;
//     case age >= 13 && age < 18:
//     console.log(firstName + ' é um adolescente!');
//     break;
//     case age >= 18 && age < 50:
//     console.log(firstName + ' é um adulto!');
//     break;
//     default:
//     console.log(firstName + ' é um idoso!');
//     break;
// }

/*____________________________________________________________________________________________________________________________________________________________*/

// Truthy e Falsy Values, e equality operators //

// var heigth;
// heigth = 23;

// if (heigth || heigth === 0) {
//     console.log('variavel foi definida');
// } else {
//     console.log('variavel não foi definida');
// }

// // equality operators
// if (heigth == '23'){
//     console.log('O operador == faz coesão de tipo!');
// }

/*____________________________________________________________________________________________________________________________________________________________*/

// // Desafio 2

// // Will:
// Fistplayer = 'Will';
// willAverageScore = (198 + 95 + 112) / 3;

// // Regi:
// Secondplayer = 'Regi';
// regiAverageScore = (187 + 86 + 109) / 3;

// // Hey:
// Thirdplayer = 'Hey';
// heyAverageScore = (136 + 91 + 105) / 3;

// if (willAverageScore > regiAverageScore && willAverageScore > heyAverageScore) {
//     console.log('O vencedor do jogo foi ' + Fistplayer + ' com uma pontuação média de ' + willAverageScore);
// } else if (regiAverageScore > willAverageScore && regiAverageScore > heyAverageScore) {     
//     console.log('O vencedor do jogo foi ' + Secondplayer + ' com uma pontuação média de ' + regiAverageScore);
// } else if (heyAverageScore > willAverageScore && heyAverageScore > regiAverageScore) {
//     console.log('O vencedor do jogo foi ' + Thirdplayer + ' com uma pontuação média de ' + heyAverageScore);
// } // Empates
//  else if (willAverageScore === regiAverageScore && willAverageScore > heyAverageScore) {
//     console.log('Ouve um empate entre os jogadores ' + Fistplayer + ' e ' + Secondplayer + ' com a pontuação média de ' + willAverageScore 
//     + ' desclassificando assim o jogador '+ Thirdplayer);
// } else if (regiAverageScore === heyAverageScore && regiAverageScore > willAverageScore) {
//     console.log('Ouve um empate entre os jogadores ' + Secondplayer + ' e ' + Thirdplayer + ' com a pontuação média de ' + regiAverageScore
//     + ' desclassificando assim o jogador '+ Fistplayer);
// } else if (heyAverageScore === willAverageScore && heyAverageScore > regiAverageScore) {
//     console.log('Ouve um empate entre os jogadores ' + Fistplayer + ' e ' + Thirdplayer + ' com a pontuação média de ' + heyAverageScore
//     + ' desclassificando assim o jogador '+ Secondplayer);
// } // Empate entre todos
//   else if (heyAverageScore === willAverageScore && heyAverageScore === regiAverageScore) {
//     console.log('Ouve um empate entre os jogadores ' + Fistplayer + ', ' + Secondplayer + ' e ' + Thirdplayer + ' com a pontuação média de ' + willAverageScore);
// }

/*____________________________________________________________________________________________________________________________________________________________*/

// // Functions
// firstName = prompt('Qual o seu nome?');
// nascimento = prompt('Qual a sua data de nascimento?');
// function calcularIdade(nascimento) {
//     return 2019 - nascimento;
// }
// idade = calcularIdade(nascimento);
// alert('Voce tem ' + idade + ' anos');

// function tempoAposentadoria(nascimento, firstName) {
//     idade = calcularIdade(nascimento);
//     aposentadoria = 70 - idade;
//     if (aposentadoria > 0) {
//         alert(firstName + ' ira se aposentar daqui ' + aposentadoria + ' anos.');
//     } else {
//         alert(firstName + ' ja tem idade o bastante para se aposentar, infelizmente ele mora no Brasil então ele se fudeu');
//     }

// }

// tempoAposentadoria(nascimento, firstName);

/*____________________________________________________________________________________________________________________________________________________________*/

// // Expressão de uma função //

// name = prompt('Qual o seu nome?')
// job = prompt('Qual o seu trabalho?')
// trabalho =  function(name, job) {
//     switch (job) {
//         case 'front-end':
//             return name + ' trabalha na area client-side.';
//         case 'back-end':
//             return name + ' trabalha na area server-side.'
//         case 'designer':
//             return name +  'trabalha na area desing.'
//         default:
//             return 'O trabalho de ' + name + ' é desconhecido.';
//     }
// }
// alert(trabalho(name, job));

/*____________________________________________________________________________________________________________________________________________________________*/

// // Array

// names = ['Will','Regi','Hey']; // Maneira mais prática de criar array;
// idades = new Array(1999, 2000, 2001); // new Array é opcional e menos prático;

// console.log(names);
// console.log(names.length);

// // Modifica um array

// names[1] = 'Reginaldo';
// names[7] = 'José';
// names[names.length] = 'Heynoan';
// console.log(names);

// // DataTypes diferentes

// will = ['Willyan', 'Fontana', 1999, 'front-end', true];
// will.push('black');
// will.unshift('Sor');
// console.log(will);

// will.pop();
// will.shift();
// console.log(will);

// console.log(will.indexOf(232));
// profissional = will.indexOf('back-end') === -1 ? 'Willyan não é um back-end': 'Willyann é um back-end';
// console.log(will.indexOf('front-end'));
// console.log(profissional);

/*____________________________________________________________________________________________________________________________________________________________*/

// // Desafio 3

// // tres restaurantes;
// // R$ 124
// // R$ 48
// // R$ 268


// function calcularGorjeta(preço) {
//     var porcentagem;

//     if (preço < 50 ){
//         porcentagem = .2;
//     } else if (preço > 50 && preço < 200) {
//         porcentagem = .15; 
//     } else if (preço >= 200) {
//         porcentagem = .1;
//     }
//     return porcentagem * preço
// }
//  preço = [115, 51, 234];
//  gorgetas = [calcularGorjeta(preço[0]),
//              calcularGorjeta(preço[1]),
//             calcularGorjeta(preço[2])];

// custoTotal = [preço[0] + gorgetas[0],
//               preço[1] + gorgetas[1],
//               preço[2] + gorgetas[2]]
// console.log(gorgetas, custoTotal);

/*____________________________________________________________________________________________________________________________________________________________*/

/* Objetos e propriedades */

// will = {
//     firstName:     'Willyan',
//     lastName:      'Fontana',
//     nascimento:     1999,
//     relachionship:  true,
//     job:           'front-end',
//     family:       ['Isadora', 'Alisson', 'Davi', 'Eleonora']
// }
// console.log(will);
// console.log(will.job);
// var n = 'relachionship';
// console.log(will[n]);

// will.job = 'developer';
// console.log(will);
/*____________________________________________________________________________________________________________________________________________________________*/

/* Objetos e metodos */

// will = {
//     firstName:     'Willyan',
//     lastName:      'Fontana',
//     nascimento:     1999,
//     relachionship:  true,
//     job:           'front-end',
//     family:       ['Isadora', 'Alisson', 'Davi', 'Eleonora'],
//     calcIdade:      function() {
//                         this.age = 2019 - this.nascimento;
//                     }
// };

// will.calcIdade();
// console.log(will);

/*____________________________________________________________________________________________________________________________________________________________*/

// // Desafio 4

// Will = {
//     Nome: 'Willyan', 
//     Massa: 70, //Kg
//     Altura:  1.83, //Metros
//     calcBMI: function () {
//         this.BMI = this.Massa/this.Altura**2;
//         return this.BMI;
//     }
// }
// Will.calcBMI();

// Regi = {
//     Nome: 'Reginaldo',
//     Massa: 70, //Kg
//     Altura: 1.83, //Metros
//     calcBMI: function () {
//         this.BMI = this.Massa/this.Altura**2;
//         return this.BMI;
//     }
// } 
// Regi.calcBMI();

//     if (Will.BMI > Regi.BMI) {
//         console.log('Com um BMI de ' + Will.BMI + ', ' + Will.Nome + ' tem um BMI maior que o do que ' + Regi.Nome + ' que tem apenas ' + Regi.BMI + ' .');
//     } else if (Will.BMI < Regi.BMI) {
//         console.log('Com um BMI de ' + Regi.BMI + ', ' + Regi.Nome + ' tem um BMI maior que o do que ' + Will.Nome + ' que tem apenas ' + Will.BMI + ' .');
//     }else if (Will.BMI === Regi.BMI) {
//         console.log('Tanto ' + Will.Nome + ' como ' + Regi.Nome + ' tem o mesmo BMI de ' + Will.BMI + ' .');
//     }

/*____________________________________________________________________________________________________________________________________________________________*/

/* Loops e interações */

// for (i = 0; i <= 100; i += 2) {
//     console.log(i);
// }

// // i = 0, 0 < 100 true, log i no console, i++
// // i = 1, 1 < 100 true, log i no console i++
// // ...
// // i = 99, 99 < 100 true, log i no console i++
// // i = 100, 100 < 100 false, fim do loop

// will = ['Willyan', 'Fontana', 1999, 'front-end', true];

// for (i = 0; i < will.length; i++){
//     console.log(will[i]);
// }

// i = 0;
// while (i < will.length) {
//     console.log(will[i]);
//     i++;
// }

// // break statement
// will = ['Willyan', 'Fontana', 1999, 'front-end', true];

// for (i = will.length; i >= 0; i--){
//     if (typeof will[i] !== 'string')
//     continue;
//     console.log(will[i]);
// }

// for (i = 0; i < will.length; i++){
//     if (typeof will[i] !== 'string')
//     break;
//     console.log(will[i]);
// }

// // loob reverso
// for (i = will.length; i >= 0; i--){
//     if (typeof will[i] !== 'string')
//     continue;
//     console.log(will[i]);
// }

/*____________________________________________________________________________________________________________________________________________________________*/

// // Desafio 5:

// // 5 restaurantes:
// // R$ 124
// // R$ 48
// // R$ 268
// // R$ 180
// // R$ 42

// will = {
//     nome:               'Willyan Fontana',
//     contas:              [124, 48, 268, 180, 42],
//     calcularGorgeta:    function() { // Essa função cria os arrays vazios para gorgeta e valor final
//                             this.gorgeta = [];
//                             this.valorFinal = [];

//                             for (i = 0; i < this.contas.length; i++) {
//                                 // Determina a porcentagem de acordo com a regra de gorgetas
//                                 var porcentagem;
//                                 conta = this.contas[i];

//                                 if (conta < 50) {
//                                     porcentagem = .2;
//                                 } else if (conta >= 50 && conta < 200) {
//                                     porcentagem = .15;
//                                 } else {
//                                     porcentagem = .1;
//                                 }

//                                 // Adiciona os resultados ao array
//                                 this.gorgeta[i] = conta * porcentagem;
//                                 this.valorFinal[i] = conta + conta * porcentagem;
//                             }
//                         }
// }

// Regi = {
//     nome:               'Reginaldo Rocha',
//     contas:              [124, 48, 268, 180, 42],
//     calcularGorgeta:    function() { // Essa função cria os arrays vazios para gorgeta e valor final
//                             this.gorgeta = [];
//                             this.valorFinal = [];

//                             for (i = 0; i < this.contas.length; i++) {
//                                 // Determina a porcentagem de acordo com a regra de gorgetas
//                                 var porcentagem;
//                                 conta = this.contas[i];

//                                 if (conta < 50) {
//                                     porcentagem = .2;
//                                 } else if (conta >= 50 && conta < 200) {
//                                     porcentagem = .15;
//                                 } else {
//                                     porcentagem = .1;
//                                 }

//                                 // Adiciona os resultados ao array
//                                 this.gorgeta[i] = conta * porcentagem;
//                                 this.valorFinal[i] = conta + conta * porcentagem;
//                             }
//                         }
// }

// function calcularMédia(gorgeta) {
//     sum = 0;
//     for (i = 0; i < gorgeta.length; i++) {
//         sum = sum + gorgeta[i]; // Serve para somar o valor total das gorgetas
                                   // EX: [2, 4, 6] -> 0 + 2 = 2| 2 + 4 = 6| 6 + 6 = 12;
//     }
//     return sum / gorgeta.length; // soma total das gorgetas dividido pelo numero de gorgetas.
// }

// // Calcula a média
// will.calcularGorgeta();
// Regi.calcularGorgeta();

// will.media = calcularMédia(will.gorgeta);
// Regi.media = calcularMédia(Regi.gorgeta);

// if (will.media > Regi.media) {
//     console.log('A familia de ' + will.nome + ' pagou mais gorgetas resultando em uma média de ' + will.media + ' .');
// } else if (will.media < Regi.media) {
//     console.log('A familia de ' + Regi.nome + ' pagou mais gorgetas resultando em uma média de ' + Regi.media + ' .')
// } else if (will.media === Regi.media){
//     console.log('Tanto a familia de ' + will.nome + ' como a de ' + Regi.nome + ' pagaram a mesma média em gorgetas, com o valor de ' + will.media + ' .');
// }