////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Hoisting

// Funções
// -------
// calcularIdade(1999);// A função funciona aqui pois é uma declaração de função e não uma expressão de função;

// function calcularIdade(year) {
//     console.log(2019 - year);
// }
// //calcularIdade(1999) // Funciona normalmente;

// // aposentadoria(1999); // A função ira dar erro se for chamada aqui pois é uma expressão de função e não uma declaração;
// aposentadoria = function (year) {
//     console.log(65 - (2019 - year));
// }
// aposentadoria(1999) // Funciona normalmente;

// // Variáveis
// // ---------
// console.log(age); // O JS está aguardando para receber uma variavel, mas ela ainda não foi definida, sendo setada para undefined;
// var age = 20;
// // console.log(age) // A variavel foi declarada;

// function foo() {
//     console.log(age) // A variavel não foi declarada então sera undefined
//     var age = 66;
//     console.log(age); // Essa variavel fica salvo no contexto de execução global.
// }
// foo();
// console.log(age);


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Exemplo de Contexto de execução:

// nome = 'Willyan'
// first();

// function first() {
//     a = 'Hello';
//     second();

//     function second() {
//         b = 'World!';
//         console.log(nome + ' says ' + a + ' ' + b);
//     }
// }




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Scope e Chain Scope:


// First scoping example

// var a = 'Oi ';
// first();

// function first() {
//     var b = 'tudo ';
//     second();

//     function second() {
//         var c = 'bem?';
//         console.log(a + b + c);
//     }
// }




// Example to show the differece between execution stack and scope chain


// var a = 'Oi, ';
// first();

// function first() {
//     var b = 'tudo ';
//     second();

//     function second() {
//         var c = 'bem?';
//         third()
//     }
// }

// function third() {
//     var d = '...';
//     console.log(a + d);
// }


///////////////////////////////////////
// Lecture: The this keyword
// console.log(this);

// calcularIdade(1999);
// function calcularIdade(ano){
//     console.log(2019 -  ano);
//     console.log(this);
// }

var will = {
    nome: 'Willyan',
    anoDoNascimento: 1999,
    calcularIdade: function(){
        console.log(this);
        console.log(2019 - this.anoDoNascimento);

        // function innerFunction(){ // Apesar dessa função estar sendo chamada dentro de um metodo ela ainda é uma regular function
        //     console.log(this); // Portanto o this ira apontar o objeto global (no caso do browser o window object)
        // }
        // innerFunction();
    }
}

will.calcularIdade();

var regi = {
    nome: 'Reginaldo',
    anoDoNascimento: 2000
};

regi.calcularIdade = will.calcularIdade;
regi.calcularIdade();