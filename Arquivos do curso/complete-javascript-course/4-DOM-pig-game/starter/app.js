/*
GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game

*/

var scores, roundScore, activePlayer, activeGameplay;

var prevRoll; // O valor do "Ultimo dado" precisa ser salvo em uma variavel global para poder ser acessado em diversos esccopos.
init();

// document.querySelector('.btn-roll').addEventListener('click', btn);

document.querySelector('.btn-roll').addEventListener('click', function () { // Uma função anonima é uma função que existe e esta sendo chamada mas não tem nome
    // Portanto não pode ser usada em um contexto fora do qual está sendo chamada;
    if (activeGameplay) {
        // 1° Gerar um número aleatório;
        var dice1 = Math.floor(Math.random() * 6) + 1;
        var dice2 = Math.floor(Math.random() * 6) + 1;
        // 2° Mostrar o resultado;
        document.getElementById('dice-1').style.display = 'block';
        document.getElementById('dice-2').style.display = 'block';
        document.getElementById('dice-1').src = 'dice-' + dice1 + '.png';
        document.getElementById('dice-2').src = 'dice-' + dice2 + '.png';
        // 3°Atualizar o score da rodada se o numero do resultado não for um;
        if (dice1 !== 1 && dice2 !== 1) {
            roundScore += dice1 + dice2;
            document.querySelector('#current-' + activePlayer).textContent = roundScore;
        } else {
            nextPlayer();
        }
    }
    // Desafio de dois 6 consecutivos zerar a pontuação
    //     if (prevRoll === 6 && dice === 6) {
    //         scores[activePlayer] = 0;
    //         document.querySelector('#score-' + activePlayer).textContent = '0';
    //         nextPlayer();
    //     } else if(dice !==1) {
    //         roundScore += dice;
    //         document.querySelector('#current-' + activePlayer).textContent = roundScore;
    //     } else {
    //         nextPlayer()
    //     }
    //     prevRoll = dice;
    // }
});

document.querySelector('.btn-hold').addEventListener('click', function () {
    if (activeGameplay) {
        // Adicionar o Score atual no Score Global
        scores[activePlayer] += roundScore;
        // scores[activePlayer] = scores[activePlayer] + roundScore; o mesmo que o de cima
        // Update o UI 
        document.querySelector('#score-' + activePlayer).textContent = scores[activePlayer];
        var input = document.querySelector('.text-score').value; // A variavel busca o valor inserido no input
        var winningScore;
        if (input) {
            winningScore = input;
        } else {
            winningScore = 100;
        }
        // Verificar se o jogador ganhou o jogo
        if (scores[activePlayer] >= winningScore) {
            document.getElementById('name-' + activePlayer).textContent = 'Vencedor!';
            document.getElementById('dice-1').style.display = 'none';
            document.getElementById('dice-2').style.display = 'none';
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            activeGameplay = false;
        } else {
            nextPlayer()
        }
    }
});

document.querySelector('.btn-new').addEventListener('click', init);

function init() {
    scores = [0, 0];
    roundScore = 0;
    activePlayer = 0;
    activeGameplay = true;
    document.getElementById('dice-1').style.display = 'none';
    document.getElementById('dice-2').style.display = 'none';
    document.getElementById('name-0').textContent = 'Player 1';
    document.getElementById('name-1').textContent = 'Player 2';
    document.getElementById('score-0').textContent = '0';
    document.getElementById('score-1').textContent = '0';
    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-0-panel').classList.add('active');

}
function nextPlayer() {
    //Next Player
    activePlayer === 0 ? activePlayer = 1 : activePlayer = 0; // Operador Ternary ? e : 
    //Se jogador ativo = 0 então jogador ativo devera ser 1 caso contrario devera ser 0.
    roundScore = 0;

    document.getElementById('current-0').textContent = '0';
    document.getElementById('current-1').textContent = '0';
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');
    document.getElementById('dice-1').style.display = 'none';
    document.getElementById('dice-2').style.display = 'none';


}

//document.querySelector('#current-' + activePlayer).textContent = dice; //  Seter - pois seta um valor
// document.querySelector('#current-' + activePlayer).innerHTML = '<b>' + dice + '</b>';
// var x = document.querySelector('#score-0').textContent; // Getter - pois ele apenas guarda o valor
// + activePlayer +